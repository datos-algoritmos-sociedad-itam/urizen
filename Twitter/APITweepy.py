# -*- coding: utf-8 -*-
"""
Created on Tue Apr  5 01:29:33 2022

@author: mcifuentes
"""

"""
LIBRERIAS 

- Se utilizarán las siguientes librerías para implementar
  la funcionalidad
"""

import flask
from flask import request
from flask_cors import CORS
import tweepy
import json
from tweepyP import search_tweet


"""
Levantamos nuestro servidor
"""
app = flask.Flask(__name__)
CORS(app)


"""
Creamos el endpoint que servirá para llamar a nuestro método que obtiene los tweets
"""

@app.route('/consulta', methods=['GET'])
def tweeter():
    # Code
    if 'q' in flask.request.args and 'n' in flask.request.args: #tbs para fecha y location para ubicación
        theme = str(flask.request.args.get('q'))
        cuantos = int(flask.request.args.get('n'))
    else:
        return "Error: No fields provided. Please specify them."

    result = search_tweet(theme, cuantos)

    return result

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)