# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 05:15:03 2022

@author: mcifuentes
"""

import tweepy
import json
import pandas as pd
import sys
# ========================= Identificarnos con API ===========================

#-------------------------- Parámetros / variables ---------------------------
# > Las primeras cuatro variables nos sirven para identificarnos con la API 
#   de Twitter.
# > Con el objeto API realizaremos todas las llamadas a Twitter desde tweepy.
# > wait_on_rate_limit & wait_on_rate_limit_notify: si llegamos al límite de 
#   búsqueda, el programa no acaba ni se cae. Espera a que haya espacio para
#   seguir descargando datos.
#-----------------------------------------------------------------------------

consumer_key = None
consumer_secret = None 
access_token = None
access_token_secret = None

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

twitter = tweepy.API(auth)
# ============================================================================ 

# =========================== Obtener tweets =================================

# Obtenemos tweets por tema

def search_tweet(theme, cuantos):
    
    """
    IMPLEMENTACIÓN
    
    --------------------------------------------------------------------------
                                    RESUMEN
    --------------------------------------------------------------------------
    
        - Se implememtará la solución para obtener los tweets deseados.
          En primer lugar, se le pedirá al usuario que ingrese la cantidad de
          tweets que desea obtener y también el usuario o hashtag deseado.

    --------------------------------------------------------------------------
                                    VARIABLES
    --------------------------------------------------------------------------
    
        - final_tweet > guarda toda la información del tweet que la API 
                        nos ofrece.
        
        - resultados > obtiene un objeto iterable con toda la información de 
                       los tweets
                       
                       + q > nombre del usuario, tema o hashtag que se quiere
                             buscar (-filter:retweets > filtro para quitar 
                             la información de los retweets)
                             
                      + n > número total de tweets que se desean obtener
                      
                      + result_type > obtenemos los tweets más recientes o los
                                      más viejos
                                      
                      + count > 
        
        - id_d > lista donde se guarda el id del tweet (cada tweet tiene uno
                                                        único)
        
        - retweet_count > lista donde se guarda la cantidad de retweets que 
                          tiene un tweet de forma individual
                          
        - screen_name > lista donde se guarda el nombre del usuario que
                        escribió el tweet
                        
        - location > lista donde se guarda la locación de donde se escribió
                     cada tweet de forma individual
        
        - followers_count > lista donde se guarda la cantidad de followers que
                            tiene la cuenta que escribió el tweet
        
        - profile_image_url > lista donde se guarda la url de la foto de
                              perfil del usuario que escribió el tweet
        
        - geo > 
        
        - coordinates > lista donde se guardan las coordenadas de donde se 
                        escribió el tweet
        
        - is_quote_status >
        
        - favorite_count > lista donde se guarda la cantidad de favoritos que
                           obtuvo un tweet de forma individual
        
        - retweeted > lista que guarda la cantidad de retweets que obtuvo un
                      tweet de forma individual
        
        - lang > lista que guarda el idioma que se utilizó al escribir un
                 tweet de forma individual
                 
        - diccionario > diccionario donde se almacenan los datos que se 
                        obtuvieron al finalizar la iteración
    """
    
    final_tweet  = None
    resultados = twitter.search_tweets(q=f'{theme}  -filter:retweets', 
                                       n = cuantos, result_type='recent', 
                                       count = cuantos)
    tweets = []
    
    id_d = []
    retweet_count = []
    screen_name = []
    location = []
    followers_count = []
    profile_image_url = []
    geo =[]
    coordinates = []
    is_quote_status = []
    favorite_count = []
    retweeted = []
    lang = []
    text = []
    urls = {"hashtags": None}
    
    # Iteramos el diccionario que tiene toda la información de los tweets
    for tweet in resultados:
        final_tweet = json.dumps(tweet._json, indent=(2))
        dic = json.loads(final_tweet)
        for key in dic:
            print(f"{key}:{dic[key]}\n")
            if key == "id":
                id_d.append(dic[key])
            if key == "text":
                text.append(dic[key])
            if key == "user":
                aux = json.dumps(dic[key], indent=2)
                aux_dic = json.loads(aux)
                for element in aux_dic:
                    #print(f"{element}:{aux_dic[element]}\n")
                    if element == "screen_name":
                        screen_name.append(aux_dic[element])
                    if element == "location":
                        location.append(aux_dic[element])
                    if element == "followers_count":
                        followers_count.append(aux_dic[element])
                    if element == "profile_image_url":
                        profile_image_url.append(aux_dic[element])
            if key == "entities":
                aux = json.dumps(dic[key], indent=2)
                aux_dic = json.loads(aux)
                #print(dic[key])
                for element in aux_dic:
                    if element == "urls":
                        urls['urls'] = aux_dic[element]
                        print(f"URLS********************: {urls}")
            if key == "geo":
                geo.append(dic[key])
            if key == "coordinates":
                coordinates.append(dic[key])
            if key == "is_quote_status":
                is_quote_status.append(dic[key])
            if key == "favorite_count":
                favorite_count.append(dic[key])
            if key == "retweet_count":
                retweeted.append(dic[key])
            if key == "lang":
                lang.append(dic[key])
            
    
    print("==============================================================")
    print("==============================================================")
    
    diccionario = {"id": id_d, "location": location, 
               "followers_count": followers_count, 
               "profile_image_url": profile_image_url, 
               "geo": geo, "coordinates": coordinates, 
               "is_quote_status": is_quote_status,
               "favorite_count": favorite_count, "retweeted": retweeted,
               "lang": lang}
    
    # Pasamos de diccionario a json
    dicci = json.dumps(diccionario, indent=2, sort_keys=False)
    print(dicci)

# ================================ Sumarizados ===============================
    
    """
    SUMARIZADOS
    
    --------------------------------------------------------------------------
                                    RESUMEN
    --------------------------------------------------------------------------
    
    - Se determinarán los datos sumarizados de cada métrica de la parte de 
      implementación
    
    --------------------------------------------------------------------------
                                    VARIABLES
    --------------------------------------------------------------------------
    
    - IDIOMAS:
        + español > guarda la cantidad de tweets que se escribieron en español
        + ingles > guarda la cantidad de tweets que se escribieron en ingles
        + other > guarda la cantidad de tweets que se escribieron en algún 
                  otro idioma que no sea español o ingles
                  
    - RETWEETS
        + min_rt > valor inicial para compararlo contra el mínimo
        + max_rt > valor inicial para compararlo contra el máximo
        + si_rt > guarda cuantos tweets obtuvieron retweet
        + count_rt > 
        + avg_rt > promedio de cuántos tweets obtuvieron retweet
        
    - FAVORITOS
        + maximo_fav > valor inicial para compararlo contra el máximo
        + minimo_fav > valor inicial para compararlo contra el mínimo
        + avg_fav > promedio de cuántos tweets obtuvieron retweet
        + count_fav > guarda cuantos tweets obtuvieron favorito
        + total_fav > total de tweets
        
    - FOLLOWERS
        + min_follow > valor inicial para compararlo contra el mínimo
        + max_follow > valor inicial para compararlo contra el máximo
        + avg_folow > promedio de cuántos followers
        + count_follow > 
        + si_follow > 
    
    - resultados > diccionario final con todos los resultados sumarizados
    """
    
    print("\nTOTAL DE TWEETS")
    print(f"Total de tweets: {cuantos}")

    # Idiomas ==============================
    español = 0
    ingles = 0
    other = 0
    
    # Iteramos sobre la lista lang
    for element in diccionario.get("lang"):
        if element == 'es':
            español += 1
        elif element == "en":
            ingles += 1
        else:
            other += 1

    # =====================================

    # RT ==================================
    min_rt = 1000000000
    max_rt = -1
    si_rt = 0
    count_rt = 0
    avg_rt = 0
    
    # Iteramos sobre la lista de retweets
    for element in retweeted:
        if element != 0:
            si_rt += 1
        if element > max_rt:
            max_rt = element
        if element < min_rt:
            min_rt = element
        count_rt += element
        
    try:
        # Determinamos el promedio de tweets con retweets
        avg_rt = si_rt / count_rt
    except:
        print("DIVISIÓN ENTRE CERO")
    
    # ========================================

    # Favorites ==============================
    maximo_fav = -1
    minimo_fav = 10000000000000000
    avg_fav = 0
    count_fav = 0
    total_fav = 0
    
    for element in favorite_count:
        if element != 0:
            count_fav += 1
        if element > maximo_fav:
            maximo_fav = element
        if element < minimo_fav:
            minimo_fav = element
        total_fav += 1
    
    try:
        avg_fav =  count_fav / total_fav
    except:
        print("DIVISIÓN ENTRE CERO")
    # ========================================

    # Followers ==============================
    min_follow = 100000000000000
    max_follow = -1
    avg_folow = 0
    count_follow = 0
    si_follow = 0
    
    # Iteramos sobre la lista de followers
    for element in followers_count:
        if element != 0:
            si_follow += 1
        if element > max_follow:
            max_follow = element
        if element < min_follow:
            min_follow = element
        count_follow += element
    try:
        # Promedio de followers
        avg_follow = si_follow / count_follow
    except:
        print("DIVISIÓN ENTRE CERO")
    # ==============================
  
# ============================================================================

    resultados = {"Tweets_espaniol": español, "Tweets_ingles": ingles,
                  "Tweets_other_language": other, "Total_retweets": count_rt,
                  "Maximo_num_retweets": max_rt, "Minimo_num_retweets": min_rt,
                  "Promedio_retweets": avg_rt, "Total_followers": count_follow,
                  "Max_followers": max_follow, "Min_followers": min_follow,
                  "Promedio_followers": avg_follow, "Total_favoritos": total_fav,
                  "Max_fav": maximo_fav, "Min_fav": minimo_fav, 
                  "Promedio_fav": avg_fav}
    
    
    # Convertimos el diccionario final a formato JSON
    resultados = json.dumps(resultados, indent=2, sort_keys=False)
    
    return resultados
#=============================================================================




















