# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 18:05:38 2022

@author: David
"""

import flask
from flask_cors import CORS
from metodos_serpapi import getGoogleImagesLinks

app = flask.Flask(__name__)
CORS(app)

@app.route('/images', methods=['GET'])
def images():
    # obtain search params:
    if 'q' in flask.request.args and 'links' in flask.request.args: 
        single_query = str(flask.request.args.get('q'))
        number_of_links = int(flask.request.args.get('links'))
        location_of_links = 'Mexico'
        to_be_searched = 'qdr:y'
    else:
        return "Error: No fields provided. Please specify them."
    
    links = getGoogleImagesLinks(single_query, number_of_links, location_of_links, to_be_searched)
    
    return flask.jsonify(links)
    
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)