# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 18:07:10 2022

@author: David
"""
import configparser
from serpapi import GoogleSearch

def getGoogleImagesLinks(single_query, number_of_links, location_of_links, to_be_searched):
    
    # obtain secret API key in credential.ini
    #it is provided by the SerpAPI webpage when creating an account
    #it's in "Your Account" from the API Dashboard after you verify your email account
    
    # config file 
    config = configparser.ConfigParser()
    config.read(r'./credentials.ini')
    # obtain credential
    serpapi_api_key = config['serpapi']['serpapi_api_key']
    
    # obtain search params    
    _search_params = {
        'q': single_query,
        'tbm': 'isch',
        'ijn': '0',
        'location': location_of_links,
        'tbs': to_be_searched,
        'api_key': serpapi_api_key
    }
    
    # search
    search=GoogleSearch(_search_params)
    results=search.get_dict()
    images_results = results['images_results']
    links={}
    i=0
    while i<number_of_links:
        obj=images_results[i]
        url=obj['original']
        links[i]=url
        i+=1
    return links


    