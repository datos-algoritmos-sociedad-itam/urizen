# API SerpAPI

* Implementación de api para ingesta de datos de Google Images utilizando [SerpAPI](https://pypi.org/project/pytrends/)

### Instalación de librerías
* Instalar las siguientes librerías:

  *  `!pip install google_images_search`
  *  `!pip install google-search-results`
  *  `!pip install flask`
  *  `!pip install -U CORS`
  *  `!pip install Flask-Cors`
  *  `!pip install configparser`

### Implementación

  * Para extraer los datos de Google Images se utilizó la librería de Python [SerpAPI](https://serpapi.com/)

  * Para usar SerpAPI, se requiere una llave única que se obtiene en el [Dashboard](https://serpapi.com/dashboard) del SerpAPI, después de crear una cuenta y verificar el correo.

  * Api creada utilizando [Flask](https://flask.palletsprojects.com/en/2.0.x/)
  
  * Se recupera los Headers de la Api con [CORS](https://pypi.org/project/Flask-Cors/)

  * Se recupera la llave especial del SerpAPI con [configparser](https://docs.python.org/3/library/configparser.html)

### Endpoint

1. Imágenes:
  * Regresa un json con las primeras N imágenes que resultan de los términos de búsqueda.
  * Endpoint: `/images`
  * Parámetros:
    * `q`: Término de búsqueda
    * `links`: Término para definir el número de imágenes que se regresan. Se pueden pedir hasta 150 imágenes.


### Ejemplos

* Ejemplo 1
  * Input: `http://127.0.0.1:5000/images?q=covid&links=10`

  * Output: `{
    "0": "https://gray-wwny-prod.cdn.arcpublishing.com/resizer/_Tjl-AorGV_fx-SGbKU56f0VdP8=/1200x675/smart/filters:quality(85)/cloudfront-us-east-1.images.arcpublishing.com/gray/NJPSGIOTCNAO3CTPJCMVFBR5GA.jpg",
    "1": "https://www.cdc.gov/museum/timeline/images/sarscov2-illus-1080x600px.jpg?_=35376",
    "2": "https://health.wyo.gov/wp-content/uploads/2020/02/23312.png",
    "3": "https://www.who.int/images/default-source/wpro/coronavirus-2.jpg?sfvrsn=f3ceff40_17",
    "4": "https://coronavirus.health.ny.gov/sites/g/files/oee1146/files/styles/mobile_lead/public/media/2021/11/girl_vaccinated_doctor_masks_1280.jpg?h=2cedd4b6&itok=yeQuSNsf",
    "5": "https://www.fda.gov/files/COVID%20testing%20policy%20drupal.jpg",
    "6": "https://media.nbcnewyork.com/2022/04/EPISODE-7-ARTICLE0.jpg?quality=85&strip=all&resize=850%2C478",
    "7": "https://www.informnny.com/wp-content/uploads/sites/58/2022/04/GettyImages-1362947872-1.jpg?w=2560&h=1440&crop=1",
    "8": "https://www.clevelandclinic.org/healthinfo/ShowImage.ashx?PIC=4480",
    "9": "https://media.npr.org/assets/img/2021/12/15/covid-neurons-1_wide-1d19c35d76fb20114061ee1d9f98407ac9e2c895.jpg?s=1400"
}`
* Ejemplo 2
  * Input: `http://127.0.0.1:5000/images?q=ucrania&links=30`

  * Output: `{
    "0": "https://ca-times.brightspotcdn.com/dims4/default/28830d1/2147483647/strip/true/crop/5000x3333+0+0/resize/2000x1333!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2Ff4%2Fbe%2Fd947d0fc4f9f8d2dceb3e2288816%2Faptopix-ukraine-invasion-67903.jpg",
    "1": "https://ca-times.brightspotcdn.com/dims4/default/15b1ee9/2147483647/strip/true/crop/6540x4360+0+0/resize/2000x1333!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2Fa5%2Fd0%2Fd33afff54b80b89d83753820aecd%2Fucrania-ojos-en-el-terreno-77824.jpg",
    "2": "https://static.dw.com/image/61135262_7.png",
    "3": "https://upload.wikimedia.org/wikipedia/commons/5/53/Mapa_ucrania.png",
    "4": "https://ak.uecdn.es/p/108/sp/10800/thumbnail/entry_id/0_3vu4or9w/version/100042/width/412/height/232",
    "5": "https://imagenes.elpais.com/resizer/tPFvodGZw2KamzWvovmujJs1y0Q=/1960x1103/cloudfront-eu-central-1.images.arcpublishing.com/prisa/YXPRQHDW4VCXVXSDL6S5DFMCPU.jpg",
    "6": "https://gdb.voanews.com/c36c0000-0aff-0242-3380-08d9ed7178f0_w1200_r1.jpg",
    "7": "https://static01.nyt.com/images/2022/03/17/world/00russia-nazis-esp1/merlin_203659248_dbb743a8-d56e-489a-a725-77ced30a1143-articleLarge.jpg?quality=75&auto=webp&disable=upscale",
    "8": "https://ca-times.brightspotcdn.com/dims4/default/1ebfe96/2147483647/strip/true/crop/8640x5760+0+0/resize/840x560!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F2b%2Fa6%2Fbc6c055ac175a951e1aff2951eb6%2Fcd5b6398b2534ed8ad6bfe79beb052f3",
    "9": "https://gdb.voanews.com/c4360000-0aff-0242-b776-08d9f617cc25_w408_r0_s.png",
    "10": "https://static01.nyt.com/images/2022/04/26/multimedia/26ukraine-blog-front-top-photos1-esp-1/26ukraine-blog-front-top-photos1-articleLarge.jpg?quality=75&auto=webp&disable=upscale",
    "11": "https://ichef.bbci.co.uk/news/640/cpsprodpb/14C3C/production/_123425058_gettyimages-1238645069.jpg",
    "12": "https://cnnespanol.cnn.com/wp-content/uploads/2022/03/220309121726-musica-violinista-banda-ucrania-full-169.jpg?quality=100&strip=info&w=1024",
    "13": "https://static01.nyt.com/images/2022/04/27/us/politics/27dc-widerwar-1-esp-1/merlin_205996773_e4e0db14-0229-4f2e-8669-d58e80c16c83-articleLarge.jpg?quality=75&auto=webp&disable=upscale",
    "14": "https://ca-times.brightspotcdn.com/dims4/default/dd6de0e/2147483647/strip/true/crop/5472x3648+0+0/resize/840x560!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F56%2Fad%2F037e6c5741026cbceb8a4cf24d95%2Fcfdj8emgq7504ddbn1ibedbziqdz0r9l7vvvyqietpuspf7agvnuuwvgxe44rbxzs7yc-2j33e2gzyntbzfcletnekm2zfzz2paqpalkxga47yj8nfqdlnjhxlz-1jqhxycwo2fqzelmmpzucezw-afvytjoxmbuqppto-rkfp6l2exo1kj41fhfvi0fnofgohfwsimmgzhsu5jop8sk17c-evkmyedcz0w4f89msomqhsm-znkmgwhj-zf5m4dey0a3vghss3yfr2zr7gt5tktx7u2ndymlo9m9baelil0r5qbw7vqlo1-ou-falomcrnfs7nnmirkqnc8jpz0vgmd-3bzk7ljxwg1k-tag-d5fxzioya8zn-6qffh-nfxipxbqxfus72qmarhpdxxbhfdecoo",
    "15": "https://ichef.bbci.co.uk/news/640/cpsprodpb/54A2/production/_124066612_mediaitem124066611.jpg",
    "16": "https://static.dw.com/image/60925799_303.jpg",
    "17": "https://img.genial.ly/5f80502e5cf06106e12fee99/7c9c6a4b-1b91-42f8-bb27-d18235c7251d.png",
    "18": "https://cnnespanol.cnn.com/wp-content/uploads/2021/12/Rusia-Ucrania.jpg?quality=100&strip=info",
    "19": "https://as01.epimg.net/diarioas/imagenes/2022/04/27/actualidad/1651035682_115064_1651062696_noticia_normal_recorte1.jpg",
    "20": "https://ichef.bbci.co.uk/news/640/cpsprodpb/0F16/production/_123426830_tv074152250.jpg",
    "21": "https://elordenmundial.com/wp-content/uploads/2022/01/mapa-politico-ucrania-2.png",
    "22": "https://static.dw.com/image/60155326_605.jpg",
    "23": "https://cnnespanol.cnn.com/wp-content/uploads/2022/04/Ayuda-militar-Ucrania-EEUU-Occidente.jpeg?quality=100&strip=info",
    "24": "https://static01.nyt.com/images/2022/04/04/world/07ukraine-nova-basan-ESP-1/04ukraine-nova-basan-top-articleLarge.jpg?quality=75&auto=webp&disable=upscale",
    "25": "https://ichef.bbci.co.uk/news/640/cpsprodpb/15AFB/production/_123972888_grid2-2x-nc.png",
    "26": "https://www.bbva.com/wp-content/uploads/2022/03/BBVA-donaciones-ucrania-1024x629.jpg",
    "27": "https://ca-times.brightspotcdn.com/dims4/default/c5489b8/2147483647/strip/true/crop/3607x2400+0+0/resize/840x559!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F4e%2F89%2F6983f48957f3d823e840ed856be6%2F444e3b2794844d7fb3c38a4932434853",
    "28": "https://estaticos-cdn.elperiodico.com/clip/3123f8d8-711a-4dcf-90a6-072d4bb640c1_alta-libre-aspect-ratio_default_0.png",
    "29": "https://f.i.uol.com.br/fotografia/2022/04/26/1651000018626842d20866a_1651000018_3x2_sm.jpg"
}`


