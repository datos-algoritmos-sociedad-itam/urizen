$('#fechaInicio').datepicker({uiLibrary: 'bootstrap4'});
$('#fechaFin').datepicker({uiLibrary: 'bootstrap4'});

window.onload = function() {
    document.getElementById('resultados').style.display = 'none';
    printEndDate();
    printTrends();
}

function printEndDate(){
    let hoy = new Date(Date.now());
    var date = hoy.getMonth()+1 + '/' + hoy.getDate() + '/' + hoy.getFullYear();
    document.getElementById('fechaFin').value = date;
}

function formatoFecha(fecha){
    let date = new Date(fecha)
    let day = `${(date.getDate())}`.padStart(2,'0');
    let month = `${(date.getMonth()+1)}`.padStart(2,'0');
    let year = date.getFullYear();
    
    return `${year}-${month}-${day}`;
}

function loadSearch(){
    let data;

    if(document.getElementById('comparacion').value === ''){
        data = {
            "single_q" : document.getElementById('busqueda').value,
            "inicio" : formatoFecha(document.getElementById('fechaInicio').value),
            "fin" : formatoFecha(document.getElementById('fechaFin').value)
        };
    } else {
        data = {
            "single_q" : document.getElementById('busqueda').value,
            "comparison_q" : document.getElementById('comparacion').value,
            "inicio" : formatoFecha(document.getElementById('fechaInicio').value),
            "fin" : formatoFecha(document.getElementById('fechaFin').value)
        };
    }

    if(data.single_q === '' || data.inicio === '' || data.fin === '')
        alert('Los campos de búsqueda, fecha inicio y fecha fin son obligatorios');
    else {
        // Vaciar campos de búsqueda
        document.getElementById('busqueda').value = '';
        document.getElementById('comparacion').value = '';
        document.getElementById('fechaInicio').value = '';
        printEndDate();

        // bloquear botón de búsqueda hasta que cargen las APIS
        document.getElementById('btnSearch').disabled = true;

        // Actualizar visibilidad de componentes 
        document.getElementById('DivTendencias').style.display = 'none';
        document.getElementById('titulo').innerHTML = 'Resultados';
        var res = 'Búsqueda por: <b> Búsqueda = ' + data.single_q + ', ';
        if(data.comparacion!='') res += 'Comparación = ' + data.comparison_q + ', ';
        res += 'Fecha de inicio = '+data.inicio+', ';
        res += 'Fecha de fin = '+data.fin;
        document.getElementById('parametros').innerHTML = res + '</b>';

        document.getElementById('resultados').style.display = 'flex';
        
        printImages(data);
        printSearchTrends(data);
        printTwitterStatitstics(data);

        // Volver a hacer clicleable el botón de búsqueda
        document.getElementById('btnSearch').disabled = false;
    }
}