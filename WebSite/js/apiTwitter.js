const url_twitter = 'http://127.0.0.1:5001';

function printTwitterStatitstics(param){
    var urlT = url_twitter + '/consulta?q=' + param.single_q + '&n=100';

    fetch(urlT, {
        method: 'GET',
        headers: new Headers({'Access-Control-Allow-Origin':'*'}),
    })
    .then((resp) => resp.json())
    .then(function(data){

        document.getElementById('tweetEsp').innerHTML = data.Tweets_espaniol;
        document.getElementById('tweetEng').innerHTML = data.Tweets_ingles;
        document.getElementById('tweetOther').innerHTML = data.Tweets_other_language;
        document.getElementById('retweetTot').innerHTML = data.Total_retweets;
        document.getElementById('retweetMax').innerHTML = data.Máximo_num_retweets;
        document.getElementById('retweetMin').innerHTML = data.Minimo_num_retweets;
        document.getElementById('retweetProm').innerHTML = data.Promedio_retweets;
        document.getElementById('followerTot').innerHTML = data.Total_followers;
        document.getElementById('followerMax').innerHTML = data.Max_followers;
        document.getElementById('followerMin').innerHTML = data.Min_followers;
        document.getElementById('followerProm').innerHTML = data.Promedio_followers;
        document.getElementById('favTot').innerHTML = data.Total_favoritos;
        document.getElementById('favMax').innerHTML = data.Max_fav;
        document.getElementById('favMin').innerHTML = data.Min_fav;
        document.getElementById('favProm').innerHTML = data.Promedio_fav;
        
    })
    .catch(function(error) {
        console.log(error);
    });
    
}