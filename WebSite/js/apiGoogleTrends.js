const url_pytrends = 'http://34.196.187.46:8000';

function printTrends(){
    fetch(url_pytrends + '/tendencias')
    .then((resp) => resp.json())
    .then(function(data){
        var html = '';
        for (var i = 0; i < Object.keys(data.tendencias_mx).length; i++)
            html+= '<h4 class="small mb-3 mt-3 font-weight-bold text-center"> '+data.tendencias_mx[i]+'</h4>';
        document.getElementById('tendencias').innerHTML = html;     
    })
    .catch(function(error) {
        console.log(error);
    });
}; 

function printSearchTrends(param){

    // TENDENCIAS GENERALES
        let urlG = new URL(url_pytrends + '/tendenciasG');
        for (let g in param) {urlG.searchParams.append(g,param[g]); }
        fetch(urlG, {
            method: 'GET',
            headers: new Headers({'Access-Control-Allow-Origin': 'http://34.196.187.46'}),
        })
        .then((resp) => resp.json())
        .then(function(res){
            tendenciasGenerales(res, param);
        })
        .catch(function(error) {
            console.log(error);
        });

    // TENDENCIAS POR REGIÓN
        let urlR = new URL(url_pytrends + '/tendenciasR');
        for (let r in param) {urlR.searchParams.append(r,param[r]); }
        fetch(urlR, {
            method: 'GET',
            headers: new Headers({'Access-Control-Allow-Origin': 'http://34.196.187.46'}),
        })
        .then((resp) => resp.json())
        .then(function(res){
            tendenciasPorRegion(res, param);
        })
        .catch(function(error) {
            console.log(error);
        });

    // SUGERENCIAS
        let urlS = new URL(url_pytrends + '/sugerencias');
        for (let s in param) {urlS.searchParams.append(s,param[s]); }
        fetch(urlS, {
            method: 'GET',
            headers: new Headers({'Access-Control-Allow-Origin': 'http://34.196.187.46'}),
        })
        .then((resp) => resp.json())
        .then(function(res){
            Sugerencias(res, param);
        })
        .catch(function(error) {
            console.log(error);
        });

    // RELACIONADOS
        let urlRel = new URL(url_pytrends + '/relacionados');
        for (let s in param) {urlRel.searchParams.append(s,param[s]); }
        fetch(urlRel, {
            method: 'GET',
            headers: new Headers({'Access-Control-Allow-Origin': 'http://34.196.187.46'}),
        })
        .then((resp) => resp.json())
        .then(function(res){
            tendenciasRelacionadas(res, param);
        })
        .catch(function(error) {
            console.log(error);
        });

}


function tendenciasGenerales(res, param){

    var tendGenChart = "";
    if(tendGenChart instanceof Chart){
        tendGenChart.clear();
        tendGenChart.destroy();
    }

    var datitos = Object.entries(res);

    const labels = [], data1 = [], data2 = [];

    datitos.forEach(element => {
        labels.push(formatoFecha(parseInt(element[0])));
        data1.push(element[1][param.single_q]);
        data2.push(element[1][param.comparison_q]);
    });
    
    const data = {
        labels: labels,
        datasets: [
            {
                label: param.single_q,
                backgroundColor: 'rgb(20, 125, 245)',
                borderColor: 'rgb(20, 125, 245)',
                data: data1,
            },
            {
                label: param.comparison_q,
                backgroundColor: 'rgb(54, 201, 100)',
                borderColor: 'rgb(54, 201, 100)',
                data: data2,
            }
        ]
    };
    
    const config = {
        type: 'line',
        data: data,
        options: {}
    };

    tendGenChart = new Chart(document.getElementById('canvasTendenciasG'),config);
    
    return tendGenChart;
}

function tendenciasPorRegion(res, param){
    const data1 = res[param.single_q], data2 = res[param.comparison_q];

    document.getElementById('TituloTendenciasRegion1').innerHTML = param.single_q;

    var busq = '';
    Object.entries(data1).forEach(element => {
        busq += '<li class="list-group-item d-flex justify-content-between align-items-center py-0">'
        +element[0]+'<span class="badge badge-primary badge-pill">'+element[1]+'</span></li>';
    });
    document.getElementById('ListaTendenciasRegion1').innerHTML = busq;

    mapa1(data1);

    if(data2 !== undefined){
        document.getElementById('TituloTendenciasRegion2').innerHTML = param.comparison_q;
    
        var busq = '';
        Object.entries(data2).forEach(element => {
            busq += '<li class="list-group-item d-flex justify-content-between align-items-center py-0">'
            +element[0]+'<span class="badge badge-primary badge-pill">'+element[1]+'</span></li>';
        });
        document.getElementById('ListaTendenciasRegion2').innerHTML = busq;

        mapa2(data2);
    }
}

function Sugerencias(res, param){
    const data1 = res['single_q_sugg'];

    document.getElementById('sugerenciasTit').innerHTML = 'Sugerencias de: ' + param.single_q;

    var html = '';
    for (var i = 0; i < Object.keys(data1).length; i++)
        html+= '<h4 class="small mb-3 mt-3 font-weight-bold text-center"> '+data1[i].title+'</h4>';
    
    document.getElementById('sugerencias').innerHTML = html;
}

function tendenciasRelacionadas(res, param){
    const data1 = res['temas_relacionados_single_query']['rising'];
    const data2 = res['temas_relacionados_single_query']['top'];

    document.getElementById('relacionadosTit').innerHTML = 'Relacionados con: ' + param.single_q;

    var html = '';
    for (var i = 0; i < Object.keys(data1).length; i++)
        html+= '<h4 class="small mb-3 mt-3 font-weight-bold text-center"> '+data1[i].query+'</h4>';
    
    document.getElementById('relacionadosRISING').innerHTML = html;

    var html1 = '';
    for (var i = 0; i < Object.keys(data2).length; i++)
        html1+= '<h4 class="small mb-3 mt-3 font-weight-bold text-center"> '+data2[i].query+'</h4>';
    
    document.getElementById('relacionadosTOP').innerHTML = html1;
}

function mapa1(data){
    $maparea1 = $(".maparea1");
	$maparea1.mapael({
		map : {
			name : "mexico",
			defaultArea : { attrs : { fill : "#dddddd", stroke: "#000000" } }
		},
        areas: arregloMapa("#147df5", data)
	});
}

function mapa2(data){
    $maparea2 = $(".maparea2");
	$maparea2.mapael({
		map : {
			name : "mexico",
			defaultArea : { attrs : { fill : "#dddddd", stroke: "#000000" } }
		},
        areas: arregloMapa("#36c964", data)
	});
}


function arregloMapa(color, data){
    return arreglo = {
        "Aguascalientes": { attrs: { fill: color, opacity : data["Aguascalientes"]/100 }, },
        "Baja California": { attrs: { fill: color, opacity : data["Baja California"]/100 }, },
        "Baja California Sur": { attrs: { fill: color, opacity : data["Baja California Sur"]/100 }, },
        "Campeche": { attrs: { fill: color, opacity : data["Campeche"]/100 }, },
        "Chiapas": { attrs: { fill: color, opacity : data["Chiapas"]/100 }, },
        "Chihuahua": { attrs: { fill: color, opacity : data["Chihuahua"]/100 }, },
        "Coahuila": { attrs: { fill: color, opacity : data["Coahuila"]/100 }, },
        "Colima": { attrs: { fill: color, opacity : data["Colima"]/100 }, },
        "Durango": { attrs: { fill: color, opacity : data["Durango"]/100 }, },
        "Guanajuato": { attrs: { fill: color, opacity : data["Guanajuato"]/100 }, },
        "Guerrero": { attrs: { fill: color, opacity : data["Guerrero"]/100 }, },
        "Hidalgo": { attrs: { fill: color, opacity : data["Hidalgo"]/100 }, },
        "Jalisco": { attrs: { fill: color, opacity : data["Jalisco"]/100 }, },
        "Mexico City": { attrs: { fill: color, opacity : data["Mexico City"]/100 }, },
        "Michoacan": { attrs: { fill: color, opacity : data["Michoacan"]/100 }, },
        "Morelos": { attrs: { fill: color, opacity : data["Morelos"]/100 }, },
        "Nayarit": { attrs: { fill: color, opacity : data["Nayarit"]/100 }, },
        "Nuevo Leon": { attrs: { fill: color, opacity : data["Nuevo Leon"]/100 }, },
        "Oaxaca": { attrs: { fill: color, opacity : data["Oaxaca"]/100 }, },
        "Puebla": { attrs: { fill: color, opacity : data["Puebla"]/100 }, },
        "Queretaro": { attrs: { fill: color, opacity : data["Queretaro"]/100 }, },
        "Quintana Roo": { attrs: { fill: color, opacity : data["Quintana Roo"]/100 }, },
        "San Luis Potosi": { attrs: { fill: color, opacity : data["San Luis Potosi"]/100 }, },
        "Sinaloa": { attrs: { fill: color, opacity : data["Sinaloa"]/100 }, },
        "Sonora": { attrs: { fill: color, opacity : data["Sonora"]/100 }, },
        "State of Mexico": { attrs: { fill: color, opacity : data["State of Mexico"]/100 }, },
        "Tabasco": { attrs: { fill: color, opacity : data["Tabasco"]/100 }, },
        "Tamaulipas": { attrs: { fill: color, opacity : data["Tamaulipas"]/100 }, },
        "Tlaxcala": { attrs: { fill: color, opacity : data["Tlaxcala"]/100 }, },
        "Veracruz": { attrs: { fill: color, opacity : data["Veracruz"]/100 }, },
        "Yucatan": { attrs: { fill: color, opacity : data["Yucatan"]/100 }, },
        "Zacatecas": { attrs: { fill: color, opacity : data["Zacatecas"]/100 }, }
    }
}