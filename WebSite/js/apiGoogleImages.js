const url_googleImages = 'http://127.0.0.1:5000';


function printImages(param){
    var lista;
    // IMÁGENES
    let urlG = new URL(url_googleImages + '/images');
    urlG.searchParams.append("q",param["single_q"]);
    urlG.searchParams.append("links",10);
    fetch(urlG, {
        method: 'GET',
        headers: new Headers({'Access-Control-Allow-Origin':'*'}),
    })
    .then((resp) => resp.json())
    .then(function(res){
        images(res);
    })
    .catch(function(error) {
        console.log(error);
    });

};

function images(res){
    var htmlista = '', img='';
    for (var i = 0; i < Object.keys(res).length; i++){
        console.log(res[i]);
        htmlista += '<div class="col-xl-10 col-lg-10 col-md-10 offset-xl-1 offset-lg-1 offset-md-1">'
        htmlista += '<img class="w-100 mx-auto my-2 rounded" src="' + res[i] + '"></div>';        
    }
    document.getElementById('listitaFotos').innerHTML = htmlista+'</div>';
};