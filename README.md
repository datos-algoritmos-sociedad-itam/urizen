Urizen
==========================================================================================================

Tablero de análisis de impacto de medios y audiencias de coyunturas y eventos políticos, sociales y económicos, acotados al territorio Mexicano.

**Partner: Centro de Investigación Económica y Presupuestaria + SocialTIC**

Justificación
-------------

Cuantificar el análisis de cómo una coyuntura, evento o noticia de interés político, social o económico es percibido o recibido por la población es un problema complicado en escala y en metodología. Los medios digitales tienen una escala y alcance que hacen que cualquier conclusión a la que lleguemos posterior a un análisis quede sesgado por la capacidad limitada de analistas humanos, mientras que el análisis asistido por máquinas permite hacerlo con mayor número de variables y pueden dejar al humano la labor de síntesis de información.

Ya existen herramientas de este tipo, pero su costo es prohibitivo para OSCs y newsrooms pequeños, por lo que este proyecto seguirá la fórmula de la "reinvención para innovación".

Planteamiento de la solución
----------------------------

Urizen será un tablero de monitoreo de coyunturas, eventos relevantes y noticias en 5 dimensiones:

1.  Google Trends (gráfica de tendencia, distribución geográfica, etc)

2.  Wordcloud de primeras 100 notas de Google News

3.  Lugar en lista de Trending Topics de TW

4.  Volumen de reacciones en TW (ponderado de likes, RTs, Quotes) por tipo de medio (texto, link, img, video)

5.  Top 30 etiquetas de imágenes de Google Image Search

El eje de tiempo será común entre todas las variables, y será dada por el mínimo común período entre todas las fuentes de datos.

El tablero será similar a la siguiente imagen. 

![](https://lh6.googleusercontent.com/CV8oHCsg99Pxrct_2_wT5eJGJGE0sMI0VWzaTzrAy6ZCCTL7ZYIYGsfdbc5uoP1xaXkUedubVnu8SuERKcbDRJz5ZNFmrblepJfPPsYHKuB46mnuZ52Sg93n4VeJth-kpTXHwK2a=s0)

Actividades:
------------

-   Diseño de BD

-   Desarrollo de ETL de Google Trends hacia Urizen

-   Pruebas de carga y performance de extractor de datos de Google Trends hacia  Urizen

-   Desarrollo de ETL de textos de Google News a Urizen

-   Desarrollo de wordcloud de textos de Google News.

-   Desarrollo de cliente de API de TW para descarga de metadata

-   Desarrollo de cliente de API de TW para descarga de tuits y replies

-   Desarrollo de ponderador de reacciones, replies y comments.

-   Pruebas de carga y performance de cliente de TW

-   Desarrollo de extractor de imgs de Google Image Search y escritura en filesystem compartido

-   Desarrollo de "pipeline" corto que lleve los resultados de Img Search a AWS Rekognition

-   Desarrollo de filtrado y selección de etiquetas

-   Escritura de etiquetas en base de datos

-   Desarrollo de filtro de tiempo para normalizar la escala para todas las fuentes

-   Desarrollo de Visualizaciones para Google Trends

-   Desarrollo de Visualizaciones para Word Cloud de Google News

-   Desarrollo de Visualizaciones para ponderado de reacciones, comments, likes

-   Desarrollo de visualizaciones para imgs y sus etiquetas

-   Desarrollo de tablero integrador

-   Pruebas de usuario

Definiciones y criterios
------------------------

### Qué es una coyuntura?

Ante el proyecto Urizen y los espacios digitales que explora, una coyuntura es una búsqueda de un término, frase, palabra (y su comparativo en el caso del espacio de Google Trends). En este sentido, los recursos de ayuda de Urizen orientarán al usuario a realizar búsquedas relevantes, pero Urizen no forzará o limitará la entrada del usuario a ciertas palabras o estructuras de lenguaje.

Recursos
--------

1.  [Google Trends API for Python. In this tutorial, I will demonstrate... | by Tanu N Prabhu | Towards Data Science](https://towardsdatascience.com/google-trends-api-for-python-a84bc25db88f)

2.  [Top 10 Best News APIs: Google News, Bloomberg, BING News and more | by Jed Ng | Rakuten RapidAPI | Medium](https://medium.com/rakuten-rapidapi/top-10-best-news-apis-google-news-bloomberg-bing-news-and-more-bbf3e6e46af6)

3.  [Google image search says api no longer available - Stack Overflow](https://stackoverflow.com/questions/34035422/google-image-search-says-api-no-longer-available) - 1er respuesta tiene el workaround

Equipo
------

-   Quién lleva el kanban board (PM)?

-   Quién le dará principalmente a la tecla (Data Scientists)?

-   Quienes le darán a la tecla part-time (Data Scientists)?

-   Quién revisa que la tecla esté bien (Mentor Técnico)?

-   Quién revisa que la tecla se apegue al problem domain (Business Specialist)?


# UPDATE (2021-10-21)

Hemos enviado una solicitud de acceso a TW para su API, con la siguiente información:

## RESEARCH PROJECT NAME
Urizen

## PROJECT DESCRIPTION
Urizen aims to be a media monitoring tool for small newsrooms and non profits to allow them to closely follow up on political and social events, and their impact across the social media space. Though there already are software solutions for this purpose, they are prohibitively costly, especially considering exchange pressures against developing countries' currencies. Upon completion, any newsroom will be able to deploy their own Urizen instance, supply it with their own TW API keys and credentials, and start leveling the field against large media conglomerates in order to make visible social phenomena at the smaller scale. The project will be licensed under the MIT License, but we will take steps to ensure our adopters are exclusively low budget newsrooms and NGO.

## DESCRIPTION OF HOW TWITTER DATA AND/OR TWITTER APIS WILL BE USED
Urizen will receive a query from the user, and then it will launch 2 workers to download data both from Google Trends API and from TW API. The query can be as open or specific as the user wishes, but so will Urizen's results be. The worker hitting the TW API will download a combination of the 200 most recent and top tweets resulting from the query, and will then be saved to a PostgreSQL database. Then, a 3rd worker will go through the tweets categorizing them and generating simple analytics from them (counts per geolocation, counts per language, counts per gender, word clouds, etc). These derived data will also be stored in the PostgreSQL database, along with the derived data obtained from Google Trends, and will be forced to match a timeline. If the user launches another query, the system will perform another search and the process will start over again. Urizen will enforce a policy of 1 query per instance every 4 hours in production environments, but while we are developing it, we may hit TW's API several times, though never obtaining more than 200 tweets.

## WILL TWITTER DATA BE PRESENTED INDIVIDUALLY OR CUMULATIVELY
Aggregate

## METHODOLOGY FOR ANALYZING TWITTER DATA, TWEETS, AND/OR TWITTER USERS
For a certain query, we will show: - count of users per location - count of messages per location - count of users per gender - count of messages per gender - place in the top trending, if available - count of likes of top tweets per post type - count of RTs of top tweets per post type These counts will then be refreshed so that after 4h we can show the % growth between when the query was launched, and when it was refreshed. All metrics will be displayed in charts in a dashboard. More info can be obtained in the public Gitlab repo: https://gitlab.com/datos-algoritmos-sociedad-itam/urizen - we will never upload the API key to Gitlab, and will remain in our infrastructure as a secret behind a Vault setup.

## HOW OUTCOMES OF YOUR RESEARCH WILL BE SHARED (INCLUDE TOOLS, DATA, AND/OR RESOURCES)
All data will be presented in an aggregate manner within Urizen's main user interface, as a dashboard, with multiple charts and graphs. The main graphical element will be the timeline, so that when users slide through it, counts, analytics and data obtained from TW as well as from Google Trends and Google Images will change with it. Charts can be exported to PNG for easy sharing in emails. End users will never have access to individual tweets or posts.
